extends CanvasLayer

signal game_started

func _on_start_pressed():
	emit_signal("game_started")
	visible = false
