extends CanvasLayer

signal play_again

func _on_restart_pressed():
	emit_signal("play_again")
