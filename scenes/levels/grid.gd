extends TileMap

const MOB = preload("res://scenes/game/mobs/mob.tscn")

var player

var turn_mob_index = -1
var player_position_init
var mobs_position_init = []
signal won
signal lost
signal finished

func _ready():
	player = get_children()[0]
	player.move_wanted.connect(_on_player_move_wanted)
	player.player_died.connect(_on_player_player_died)
	player.turn_finished.connect(_on_player_turn_finished)
	
	player.set_camera(self)
	
	player_position_init = player.position
	var mobs = get_tree().get_nodes_in_group("mob")
	for mob in mobs:
		mob.set_grid(self)
		mob.turn_finished.connect(_on_mob_turn_finished)
		mobs_position_init.push_back(mob.position)

func reset():
	## Init player
	player.position = player_position_init
	player.reset()
	player.show()

	## Kill all remaning mobs
	var mobs = get_tree().get_nodes_in_group("mob")
	for mob in mobs:
		mob.queue_free()

	## Spawn new mobs
	for mob_position in mobs_position_init:
		var mob = MOB.instantiate()
		mob.position = mob_position
		mob.add_to_group("mob")
		mob.set_grid(self)
		mob.turn_finished.connect(_on_mob_turn_finished)
		add_child(mob)

func _on_player_move_wanted(player, direction):
	var position_player = local_to_map(player.position)
	var target = position_player + direction

	## Detect decoration
	var decoration = get_cell_tile_data(2, target)
	if decoration != null && decoration.get_custom_data("chest") == true:
		emit_signal("won")
		return

	## Detect doors
	var door = get_cell_tile_data(3, target)
	if door != null:
		emit_signal("finished")
		return

	## Detect mob
	var mobs = get_tree().get_nodes_in_group("mob")
	for mob in mobs:
		var position_mob = local_to_map(mob.position)
		if position_mob == target:
			player.attack(direction, mob)
			return

	var cell_walls = get_cell_atlas_coords(1, target)
	var cell_decorations = get_cell_atlas_coords(2, target)
	if cell_walls == Vector2i(-1, -1) && cell_decorations == Vector2i(-1, -1):
		player.move(direction)
	else:
		player.can_play = true

func mobs_turn(player):
	turn_mob_index += 1
	var mobs = get_tree().get_nodes_in_group("mob")

	if mobs.size() == turn_mob_index:
		print("new turn")
		player.can_play = true
		turn_mob_index = -1
		return

	var position_player = local_to_map(player.position)
	var mob = mobs[turn_mob_index]
	var position_mob = local_to_map(mob.position)
	mob.move(position_mob, position_player, player)

func pick_up_item(position_end_turn):
	## Detect worm
	var worms = get_tree().get_nodes_in_group("worm")
	for worm in worms:
		var position_worm = local_to_map(worm.position)
		if position_worm == position_end_turn:
			player.pick_up("red_worm")
			worm.queue_free()
			return

func _on_player_turn_finished(player):
	pick_up_item(local_to_map(player.position))
	mobs_turn(player)

func _on_mob_turn_finished():
	mobs_turn(player)

func _on_player_player_died():
	emit_signal("lost")
