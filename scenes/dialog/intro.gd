extends Node2D

@onready var dialog_box = $DialogBox
@onready var speech = $DialogBox/Speech
@onready var player = $Player
@onready var walk = $Walk
@onready var animation_player = $AnimationPlayer

signal finished

var player_step = 5
var dialogue_index = 0
var dialogues = [
	"Help... me...",
	"I am here... on the path...",
	"",
	"I am a wizard from the nearby city",
	"Young man, some bandit stole my staff !",
	"Go in there place and get back my stuff"
]

func _ready():
	player.hide_HUD()

func start():
	walk.start()

func _physics_process(_delta):
	if dialog_box.visible == false or dialogue_index == 7:
		return
	if Input.is_action_just_pressed("ui_select"):
		next_dialog()

func _on_timer_timeout():
	player_step -= 1
	if player_step == 0:
		walk.stop()
		start_dialog()
	player.move(Vector2i(1, 0))

func start_dialog():
	dialog_box.visible = true
	next_dialog()

func next_dialog():
	if dialogue_index == 6:
		emit_signal("finished")
		return

	if dialogue_index == 2:
		dialog_box.visible = false
		player_step = 3
		walk.start()
	if dialogue_index == 3:
		dialog_box.reveal()
	dialog_box.set_text(dialogues[dialogue_index])
	dialog_box.start_dialogue()
	if dialogue_index == 5:
		animation_player.play("end")
	dialogue_index += 1

func _on_animation_player_animation_finished(anim_name):
	emit_signal("finished")
