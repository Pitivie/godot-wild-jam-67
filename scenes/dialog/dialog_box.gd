extends CanvasLayer

@onready var speech = $Panel/Speech
@onready var wizard = $Panel/Wizard

# Called when the node enters the scene tree for the first time.
func set_text(text):
	speech.text = text 

func start_dialogue():
	speech.visible_ratio = 0
	var tween = get_tree().create_tween()
	tween.tween_property(speech, "visible_ratio", 1, 2)

func reveal():
	wizard.modulate = Color("ffffff")
