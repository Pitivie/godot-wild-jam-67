extends Node2D

@onready var game = $Game
@onready var intro = $Intro

var intro_is_finished = false

func _physics_process(_delta):
	if intro_is_finished and game.visible != true and Input.is_action_just_pressed("ui_select"):
		game.start_init()
		game.visible = true
		intro.queue_free()

func _on_welcome_game_started():
	intro.start()

func _on_intro_finished():
	intro_is_finished = true
