extends Node2D

@onready var victory = $Victory
@onready var defeat = $Defeat

var current_level_index = 0
var levels = [
	preload("res://scenes/levels/level_one.tscn"),
	preload("res://scenes/levels/level_two.tscn")
]
var level
var grid

func start_init():
	start()
	$DongeonAudio.play()

func start():
	level = levels[current_level_index].instantiate()
	add_child(level)
	grid = level.get_child(0)
	grid.won.connect(_on_level_one_won)
	grid.lost.connect(_on_level_one_lost)
	grid.finished.connect(_on_level_finished)

func _on_level_one_won():
	victory.show()

func _on_victory_play_again():
	victory.hide()
	current_level_index = 0
	level.queue_free()
	start()

func _on_level_one_lost():
	defeat.show()
	$DongeonAudio.stop()
	$DefeatAudio.play()

func _on_defeat_play_again():
	grid.reset()
	defeat.hide()
	$DefeatAudio.stop()
	$DongeonAudio.play()

func _on_level_finished():
	current_level_index += 1
	level.queue_free()
	start()
