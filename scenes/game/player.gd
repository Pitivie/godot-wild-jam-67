extends Node2D

const RED_WORM = preload("res://scenes/game/player/worm_effect/red_worm.tscn")
const WORM = preload("res://scenes/game/player/worm_effect/worm.tscn")

@onready var character = $Character
@onready var animation_player = $AnimationPlayer
@onready var inventory = $Inventory
@onready var eat_red_worm = $Character/EatRedWorm
@onready var life_bar = $LifeBar
@onready var camera_2d = $Camera2D

var can_play = true

signal move_wanted
signal turn_finished
signal player_died

const MAX_HEALTH = 10
var current_health = MAX_HEALTH
var attack_point = 1
var number_turns_remaining_worm = 0
var worm = WORM.instantiate()

func _ready():
	life_bar.set_life_max(MAX_HEALTH)
	life_bar.set_life(current_health)

func reset():
	current_health = MAX_HEALTH
	can_play = true
	worm = WORM.instantiate()
	life_bar.set_life_max(MAX_HEALTH)
	life_bar.set_life(current_health)

func _physics_process(_delta):
	if !can_play:
		return

	if Input.is_action_just_pressed("action_1"):
		if !inventory.remove("red_worm"):
			return
		eat_worm("red_worm")
		return

	var motion = Vector2i()

	if Input.is_action_just_pressed("ui_up"):
		motion.y -= 1
		can_play = false
		emit_signal("move_wanted", self, motion)
	if Input.is_action_just_pressed("ui_down"):
		motion.y += 1
		can_play = false
		emit_signal("move_wanted", self, motion)
	if Input.is_action_just_pressed("ui_left"):
		motion.x -= 1
		character.flip_h = true
		can_play = false
		emit_signal("move_wanted", self, motion)
	if Input.is_action_just_pressed("ui_right"):
		motion.x += 1
		character.flip_h = false
		can_play = false
		emit_signal("move_wanted", self, motion)

func move(direction):
	var new_position = position + direction * 16.0
	$WalkSfx.play()
	animation_player.play("walk_1")
	var tween = get_tree().create_tween()
	tween.tween_property(self, "position", new_position, 0.5)

func _on_animation_player_animation_finished(anim_name):
	if anim_name != "get_hurt":
		finish_turn()

func attack(direction, mob):
	## Direction for animation
	if direction.x > 0:
		animation_player.play("attack_right")
	elif direction.x < 0:
		animation_player.play("attack_left")
	elif direction.y > 0:
		animation_player.play("attack_bottom")
	elif direction.y < 0:
		animation_player.play("attack_top")
	
	$Attack.play()
	## Damage with worm bonus
	mob.take_dammage(attack_point)
	mob.hurt()

func take_dammage(amount):
	current_health -= amount
	$GetHitSfx.play()
	animation_player.play("get_hurt")
	life_bar.set_life(current_health)
	if current_health == 0:
		emit_signal("player_died")

func pick_up(item):
	$PickUpWorm.play()
	inventory.add(item)

func finish_turn():
	worm.malus_by_turn(self)
	number_turns_remaining_worm -= 1
	if number_turns_remaining_worm == 0:
		worm.reset(self)
		worm = WORM.instantiate()
		eat_red_worm.hide()
	emit_signal("turn_finished", self)

func eat_worm(type):
	$EatWorm.play()
	eat_red_worm.show()
	eat_red_worm.play("default")
	worm = RED_WORM.instantiate()
	worm.boost(self)
	can_play = false

func _on_eat_red_worm_animation_finished():
	finish_turn()

func hide_HUD():
	inventory.visible = false
	life_bar.visible = false

func set_camera(grid: TileMap):
	var mapRect = grid.get_used_rect()
	var tileSize = grid.cell_quadrant_size
	var worldSizeInPixels = mapRect.size * tileSize
	camera_2d.limit_right = worldSizeInPixels.x
	camera_2d.limit_bottom = worldSizeInPixels.y
