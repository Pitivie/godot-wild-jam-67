extends CanvasLayer

@onready var texture_progress_bar = $TextureProgressBar

func set_life_max(max):
	texture_progress_bar.max_value = max

func set_life(value):
	texture_progress_bar.value = value
