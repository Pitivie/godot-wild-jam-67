extends Node

var stock = {
	"red_worm": 0
}

@onready var counter = {
	"red_worm": $RedWormSlot/Slot/Counter
}

@onready var slot = {
	"red_worm": $RedWormSlot/Slot
}

func reset():
	stock = {
		"red_worm": 0
	}

func add(item):
	var slot = {
		"red_worm": $RedWormSlot/Slot
	}
	
	stock[item] += 1
	if stock[item] > 0:
		slot[item].visible = true
	counter[item].text = str(stock[item])

func remove(item):
	if stock[item] == 0:
		return false
	stock[item] -= 1
	if stock[item] == 0:
		slot[item].visible = false
	counter[item].text = str(stock[item])
	return true
