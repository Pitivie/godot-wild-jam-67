extends "res://scenes/game/player/worm_effect/worm.gd"

var init_attack

func boost(player):
	init_attack = player.attack_point
	player.attack_point = 2
	player.number_turns_remaining_worm = 5

func malus_by_turn(player):
	player.take_dammage(1)

func reset(player):
	player.attack_point = init_attack
