extends Node2D

@onready var progress_bar = $ProgressBar
@onready var animation_player = $AnimationPlayer
@onready var visible_on_screen_notifier_2d = $VisibleOnScreenNotifier2D

const MAX_HEALTH = 2
const ATTACK_POINT = 1

var current_health = MAX_HEALTH
var navigation
var grid

signal attacked_player
signal turn_finished

func set_grid(given_grid: TileMap):
	grid = given_grid

func refresh_nav():
	navigation = AStarGrid2D.new()
	navigation.diagonal_mode = AStarGrid2D.DIAGONAL_MODE_NEVER
	navigation.region = grid.get_used_rect()
	navigation.cell_size = Vector2(16, 16)
	navigation.update()

	## Add obstacle: walls
	var walls = grid.get_used_cells(1)
	for wall in walls:
		navigation.set_point_solid(wall, true)

	## Add obstacle: other mobs
	var mobs = get_tree().get_nodes_in_group("mob")
	for mob in mobs:
		if get_instance_id() == mob.get_instance_id():
			continue
		navigation.set_point_solid(grid.local_to_map(mob.position), true)

	## Add obstacle: decoration
	var decorations = grid.get_used_cells(2)
	for decoration in decorations:
		navigation.set_point_solid(decoration, true)

func move(position_on_grid: Vector2i, target: Vector2i, player):
	if !visible_on_screen_notifier_2d.is_on_screen():
		turn_finish()
		return

	refresh_nav()

	var nav_list = navigation.get_point_path(position_on_grid, target)

	if nav_list.size() > 2:
		var new_position = nav_list[1] + Vector2(8, 8)
		animation_player.play("walk")
		var tween = get_tree().create_tween()
		tween.tween_property(self, "position", new_position, 0.5)
	elif nav_list.size() <= 2:
		attack(position_on_grid - target)
		player.take_dammage(ATTACK_POINT)

func turn_finish():
	emit_signal("turn_finished")

func attack(direction):
	if direction.x < 0:
		animation_player.play("attack_right")
	elif direction.x > 0:
		animation_player.play("attack_left")
	elif direction.y > 0:
		animation_player.play("attack_bottom")
	elif direction.y < 0:
		animation_player.play("attack_top")

func hurt():
	animation_player.play("get_hurt")

func take_dammage(amount):
	current_health -= amount
	progress_bar.value = amount
	if current_health == 0:
		queue_free()

func _on_animation_player_animation_finished(anim_name):
	if anim_name != "get_hurt":
		turn_finish()
